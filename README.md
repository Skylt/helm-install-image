# Helm install image

Minimal images with [Helm](https://helm.sh/) and [kubectl](https://v1-15.docs.kubernetes.io/docs/tasks/tools/install-kubectl/) installed. Built for GitLab's cluster integration.

There are three variants:


## Helm v3

Contains Helm 3 and kubectl

### Binaries

- Helm v3:
  - `/usr/bin/helm`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/3.7.1-kube-1.20.11-alpine-3.14
```

## Helm 2to3

Contains both Helm v2 and Helm v3, as well as the `helm-2to3` plugin installed on Helm v3 to facilitate Helm v3 migrations.

### Binaries

- Helm v2:
  - `/usr/bin/helm2`
  - `/usr/bin/tiller`
- Helm v3:
  - `/usr/bin/helm3`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/helm-2to3-2.17.0-3.7.1-kube-1.20.11-alpine-3.14
```

## Helm 2 [DEPRECATED]

**WARNING:** The Helm v2 version of the image is deprecated and no longer actively updated as part of our CI/CD pipelines. The release listed below is the _very last one_. If you need
a more recent image that includes Helm v2, you can use the [Helm 2to3](#helm-2to3) variant.

Contains Helm v2 and kubectl

### Binaries

- Helm v2:
  - `/usr/bin/helm`
  - `/usr/bin/tiller`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.17.0-kube-1.16.15-alpine-3.14
```
